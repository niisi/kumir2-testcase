#!/bin/bash

DIRS="kumir_prog NewErrors new_files new_standards newTOS tOurNew"

for d in $DIRS
do
    find $d -executable -type f -delete
done